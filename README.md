# Usage

This assumes that you are using go mod to build.

```shell
docker run --rm -v "$PWD":/myrepo -w /myrepo lunny/centos-go:latest go build
```